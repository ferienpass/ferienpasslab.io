Software entwickeln für Mädchen & Jungs!
========================================

[Ferienpass Kreuzlingen](https://www.ferienpass-kreuzlingen.ch/)
------------------------

Dieses Repository enthält den Quellcode der Kurs-Website 2020-2021 (Kurs #43).

Website
-------

- https://ferienpass.gitlab.io/
- https://ferienpass.gitlab.io/blog/
- https://ferienpass.gitlab.io/2020/
